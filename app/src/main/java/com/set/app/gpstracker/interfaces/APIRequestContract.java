package com.set.app.gpstracker.interfaces;

/**
 * Created by kennethyeh on 2017/3/22.
 */

public interface APIRequestContract {

    interface View {
        void onRequestResult(Object resParam);
    }

    interface Presenter {
        void startRequest(Object reqParam);
        void stopRequest();
    }
}
