package com.set.app.gpstracker.util.connect;

import android.os.Handler;
import android.util.Log;

/**
 * Created by kennethyeh on 2017/3/22.
 */

public class ThreadConnector {
    private static final String TAG = "ThreadConnector";

    private CallBack mCallBack;
    private RequestPara mRequestPara;
    private boolean mResponseParsing = false;

    private Thread mThread ;
    private int httpStatus;

    public interface CallBack{
        void doCallBack(Object obj);
    }

    public ThreadConnector(CallBack mCallBack){
        this.mCallBack 			= mCallBack;
    }

    public void startRequest(RequestPara requrestPara){
        this.mRequestPara 		= requrestPara;
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                int requestMethod = mRequestPara.httpMethod;

                if(requestMethod == RequestPara.HTTP_METHOD_GET ){
                    HttpGetUtil connectGet = new HttpGetUtil();
                    mRequestPara = connectGet.getHttpStatus(mRequestPara);
                }

                if(requestMethod == RequestPara.HTTP_METHOD_POST ){
                    HttpPostUtil connectPost = new HttpPostUtil();
                    mRequestPara = connectPost.getHttpStatus(mRequestPara);
                }
                parsingResponse();
            }
        });
        mThread.start();
    }

    private void parsingResponse(){
        if(!mResponseParsing){
            doCallback(mRequestPara);
        }else{
            startParsing();
        }
    }

    private void startParsing(){

        doCallback(mRequestPara);
    }

    private Handler mHandler = new Handler();
    private void doCallback(final RequestPara mRequestPara){
        //  Only the original thread that created a view hierarchy can touch its views
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "stopFlag:"+stopFlag);
                if(stopFlag){
                    return;
                }
                mCallBack.doCallBack(mRequestPara);
            }
        });
    }

    private boolean stopFlag = false;
    public void stop(){
        stopFlag = true;
    }
}

