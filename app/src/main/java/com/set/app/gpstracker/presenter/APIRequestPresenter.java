package com.set.app.gpstracker.presenter;

import android.content.Context;

import com.set.app.gpstracker.interfaces.APIRequestContract;
import com.set.app.gpstracker.util.connect.RequestPara;
import com.set.app.gpstracker.util.connect.ThreadConnector;

/**
 * Created by kennethyeh on 2017/3/22.
 */

public class APIRequestPresenter implements APIRequestContract.Presenter{


    private static final String TAG     = "APIRequestPresenter";
    private Context mContext;

    private APIRequestContract.View mView;
    private ThreadConnector mThreadConnector ;

    public APIRequestPresenter(Context context, APIRequestContract.View view) {
        mContext        = context;
        this.mView      = view;
        mThreadConnector = new ThreadConnector(mCallBack);
    }
    @Override
    public void startRequest(Object reqParam) {
        RequestPara mRequestPara = (RequestPara) reqParam;
        mThreadConnector.startRequest(mRequestPara);
    }

    @Override
    public void stopRequest() {
        if(mThreadConnector!=null){
            mThreadConnector.stop();
        }
    }

    ThreadConnector.CallBack mCallBack = new ThreadConnector.CallBack() {
        @Override
        public void doCallBack(Object obj) {
            RequestPara mRequestPara = (RequestPara) obj;
            mView.onRequestResult(mRequestPara);
        }
    };
}
